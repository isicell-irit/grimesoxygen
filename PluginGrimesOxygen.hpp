#ifndef PLUGINGRIMESOXYGEN_HPP
#define PLUGINGRIMESOXYGEN_HPP

/**
 * @file PluginGrimesOxygen.hpp
 * @brief Defines the PluginGrimesOxygen class for spherical oxygen diffusion.
 *
 * Contains the spherical oxygen diffusion-based plugin class.
 */

#include <vector>
#include <mecacell/mecacell.h>
#include "../SpheroidManager/PluginSpheroidManager.hpp"
#include <math.h>

/**
 * @namespace GrimesOxygen
 * @brief Namespace for oxygen diffusion-related classes and functions.
 */
namespace GrimesOxygen {

    /**
     * @class PluginGrimesOxygen
     * @brief Class for managing spherical oxygen diffusion.
     * 
     * @tparam cell_t Type of the cell.
     */
    template<typename cell_t>
    class PluginGrimesOxygen {

    private:
        double a = 5.634224/10.0; /**< Oxygen consumption in 10^-7 m^3/kg/s = 10^-1 µm^3/ng/s */
        double omega = 10.0*3.0318; /**< Henry constant for oxygen at human temperature multiplied by density of a tumor and density of oxygen in 10^7 mmHg.kg/m^3 = 10 mmHg.ng/µm^3 */
        double D = 2000.0; /**< Diffusion constant in 10^-12 m^2/s = µm^2/s */
        double po = 37.50319; /**< External partial pressure of oxygen in mmHg */
        SpheroidManager::PluginSpheroidManager<cell_t> *spheroidManager = nullptr;

        /**
         * @brief Updates the oxygen levels for the given cells.
         * 
         * @param cells Vector of cell pointers.
         */
        void updateOxygen(std::vector<cell_t*> cells) {
            double rl = sqrt(6. * D * po / (a * omega)); // Limit radius in µm
            double rn = 0; // Necrotic radius in µm
            double cstConsoDiff = a * omega / (6. * D); // in mmHg/µm^2
            if (spheroidManager->getSpheroidRadius() > rl)
                rn = (spheroidManager->getSpheroidRadius() * (1.0f / 2.0f - cos((acos(1.0f - 2.0f * pow(rl, 2) / pow(spheroidManager->getSpheroidRadius(), 2)) - 2.0f * M_PI) / 3.0f)));

            for (auto &c : cells) {
                c->getBody().setOxygen(eqOxygen(cstConsoDiff, rn, c->getBody().getDistanceFromCentroid()));
            }
        }

        /**
         * @brief Computes the partial pressure of oxygen for a cell.
         * 
         * @param cstConsoDiff Constant for consumption and diffusion.
         * @param rn Necrotic radius.
         * @param r Distance from the centroid.
         * @return Partial pressure of oxygen.
         */
        double eqOxygen(double cstConsoDiff, double rn, double r) {
            double pr = 0;
            if (r < rn) {
                return 0.0;
            } else if (r >= spheroidManager->getSpheroidRadius()) {
                return po;
            } else {
                if (r == 0)
                    pr = po + cstConsoDiff * (pow(r, 2) - pow(spheroidManager->getSpheroidRadius(), 2));
                else
                    pr = po + cstConsoDiff * (pow(r, 2) - pow(spheroidManager->getSpheroidRadius(), 2) + 2.0f * pow(rn, 3) * ((1 / r) - (1 / spheroidManager->getSpheroidRadius())));
                if (pr >= 0)
                    return pr;
                else
                    return 0.0;
            }
        }

    public:
        /**
         * @brief Default constructor.
         */
        inline PluginGrimesOxygen() = default;

        /**
         * @brief Constructor with spheroid manager.
         * 
         * @param sm Pointer to the spheroid manager.
         */
        inline PluginGrimesOxygen(SpheroidManager::PluginSpheroidManager<cell_t> *sm) : spheroidManager(sm) {}

        /**
         * @brief Sets the external partial pressure of oxygen.
         * 
         * @param p External partial pressure of oxygen.
         */
        inline void setPo(double p) { po = p; }

        /**
         * @brief Sets the oxygen consumption rate.
         * 
         * @param c Oxygen consumption rate.
         */
        inline void setOxygenConsumption(double c) { a = c; }

        /**
         * @brief Sets the diffusion constant.
         * 
         * @param d Diffusion constant.
         */
        inline void setDiffusionConstant(double d) { D = d; }

        /**
         * @brief Sets the Henry constant for oxygen.
         * 
         * @param om Henry constant for oxygen.
         */
        inline void setOmega(double om) { omega = om; }

        /**
         * @brief Pre-behavior update hook for MecaCell.
         *
         * Updates oxygen quantities.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void preBehaviorUpdate(world_t *w) {
            if(!spheroidManager) spheroidManager = &w->cellPlugin.pluginSpheroidManager;
            if (w->cells.size() > 5) {
                updateOxygen(w->cells);
            }
        }
    };
}

#endif // PLUGINGRIMESOXYGEN_HPP
