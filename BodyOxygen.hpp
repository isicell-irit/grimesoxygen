#ifndef BODYOXYGEN_HPP
#define BODYOXYGEN_HPP

#include "../../../../src/core/BaseBody.hpp"

/**
 * @file BodyOxygen.hpp
 * @brief Defines the BodyOxygen class for spherical oxygen diffusion.
 *
 * Contains the body class which works with the GrimesOxygen plugin.
 */

/**
 * @namespace GrimesOxygen
 * @brief Namespace for oxygen diffusion-related classes and functions.
 */
namespace GrimesOxygen {

    /**
     * @class BodyOxygen
     * @brief Class for managing spherical oxygen diffusion in a cell body.
     * 
     * @tparam cell_t Type of the cell.
     * @tparam plugin_t Type of the plugin.
     */
    template<typename cell_t, class plugin_t>
    class BodyOxygen : public virtual BaseBody<plugin_t> {

    private:
        double oxygen; /**< Oxygen quantity in mmHg */

    public:

        /**
         * @brief Default constructor.
         */
        inline BodyOxygen() = default;

        /**
         * @brief Constructor with initial oxygen quantity.
         * 
         * @param o2 Initial oxygen quantity.
         */
        inline BodyOxygen(double o2) : oxygen(o2) {}

        /**
         * @brief Gets the oxygen quantity.
         * 
         * @return Oxygen quantity.
         */
        inline double getOxygen() const { return oxygen; }

        /**
         * @brief Sets the oxygen quantity.
         * 
         * @param value Oxygen quantity.
         */
        inline void setOxygen(const double value) { oxygen = value; }

    };

}
#endif // BODYOXYGEN_HPP
