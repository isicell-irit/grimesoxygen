# Grimes Oxygen 

This folder contains the body and plugin classes that could be used to simulate a spherical diffusion of oxygen applied on cells using *MecaCell*.
Most of the values and equations are based on [David Robert Grimes work](https://doi.org/10.1098/rsif.2013.1124) on tumor spheroids.

It was tested on Debian 10.

## Prerequisites

- work on 3D *MecaCell* simulation
- have a general body class that will inherit the **BodyOxygen** class, this class also needs to inherit the *MecaCell* **Movable** class
- have a general plugin class that will use the **PluginGrimesOxygen** class
- use the *SpheroidManager* module in your model

## How to use it

- include the folder in your *MecaCell* project  
- import the **BodyOxygen.hpp** file in the file containing your simulation general body class  
- make your general body class inherit from the **BodyOxygen** class
```cpp 
template <class cell_t> class CellBody : public BodyOxygen
 ```  
- in your general body class constructor add the following line :
 ```cpp 
 explicit CellBody(cell_t *, MecaCell::Vec pos = MecaCell::Vec::zero())
 : BodyOxygen(oxygen)
 ```
 oxygen corresponds to the default value of oxygen in the environment in **mmHg**
- import the **PluginGrimesOxygen.hpp** file in the file containing your general plugin class or struct  
- add a **PluginGrimesOxygen** attribute to your  general plugin class or struct  
```cpp  
PluginGrimesOxygen oxygenPlugin = pluginGrimesOxygen(smPlugin);  
```  
smPlugin corresponds to the instance of **PluginSpheroidManager** used in the model 
- in the onRegister method, add the new attribute (make sure to register the spheroid manager *BEFORE* the oxygen plugin)
```cpp  
template <typename world_t> void onRegister(world_t* w){  
    ...
    w->registerPlugin(oxygenPlugin); 
}  
```
Your simulation will now implement cells that have their oxygen quantity according to their distance from the center of the spheroid.

## How to calibrate your simulation

### Oxygen Consumption

You can modify the oxygen consumption of the spheroid by using the a setter.
You'll have to access it from the world in your scenario at initialization.
```cpp  
w.cellPlugin.oxygenPlugin.setOxygenConsumption(0.5);  
```
The default value is set to **0.5634224 µm³/ng/s**. That means that a 1 ng spheroid will consume 0.5634224 µm³ of oxygen per second.

### Omega

You can modify the omega constant.
You'll have to access it from the world in your scenario at initialization.
```cpp  
w.cellPlugin.oxygenPlugin.setOmega(30.);  
```
The default value is set to **30.318 mmHg.ng/µm³**.
You can find more information about the omega constant by reading *GRIMES, David Robert, KELLY, Catherine, BLOCH, Katarzyna, et al. A method for estimating the oxygen consumption rate in multicellular tumour spheroids. Journal of The Royal Society Interface, 2014, vol. 11, no 92, p. 20131124. -> consommation oxygene* article section 2.

### Diffusion Constant

The diffusion constant is settable.
You'll have to access it from the world in your scenario at initialization.
```cpp   
w.cellPlugin.oxygenPlugin.setDiffusionConstant(1500.);      
```
The default value is set to **2000.0 µm²/s**.

### External Partial Pressure in Oxygen

You can set the partial pressure in Oxygen of the environment.
```cpp   
w.cellPlugin.oxygenPlugin.setPo(42.);      
```
The default value is set to **37.50319 mmHg** corresponding to the oxygenation of cells at the human body temperature.

